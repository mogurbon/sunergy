import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { localize } from 'vee-validate';
import es from 'vee-validate/dist/locale/es.json';

Vue.prototype.$http = axios
Vue.config.productionTip = false
localize('es', es);


new Vue({
  render: h => h(App),
}).$mount('#app')
